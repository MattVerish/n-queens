#This is going to be a file for testing a few ways to
#solve the N-Queens problem

import numpy as np
import random


class Board():

	def __init__(self, size):
		self.board_size = size
		self.board = np.zeros((size, size))
		for i in range(size):
			loc = random.randint(0, size-1)
			self.board[i][loc] = 1


	#Solves N-Queens by repeatedly calling the stepIterative function until no
	#constraints are violated.
	def solve(self):
		while self.max_violations()[0] > 0:
			self.stepIterative()

	#Make one move in iterative improvement algorithm. Moves queen that violates
	#the most constraints to the best possible location.
	def stepIterative(self):
		worstQueen = self.max_violations()[-1]
		bestPlacement = self.min_placement(worstQueen)
		queenLoc = np.argmax(self.board[worstQueen])
		self.board[worstQueen][queenLoc] = 0
		self.board[worstQueen][bestPlacement] = 1

	#Returns the number of constraints violated by the queen at position (i, x)
	def violated_constraints(self, i, x):
		board = self.board
		violated = sum(board[:,x]) + np.trace(board, offset=(x-i)) + sum(np.rot90(board).diagonal(i-board.shape[0]+1 + x)) - 3
		return int(violated)

	#Returns the queen that violates the most constraints and how many constrains it violates
	# breaks ties randomly
	def max_violations(self):
		board = self.board
		maxV = 0
		maxI = 0
		nums = [i for i in range(board.shape[0])]
		random.shuffle(nums)
		for i in nums:
			j = np.argmax(board[i])
			violations = self.violated_constraints(i, j)
			if violations > maxV:
				maxV = violations
				maxI = i
		return maxV, maxI

	#Calculates the column that would violate the least constraints if you moved queen i to it
	def min_placement(self, i):
		board = self.board
		currentPos = np.argmax(board[i])
		board[i][currentPos] = 0
		nums = [i for i in range(board.shape[0])]
		random.shuffle(nums)
		minV = board.shape[0]
		minC = 0
		for j in nums:
			if j == currentPos:
				continue
			board[i][j] = 1
			currentV = self.violated_constraints(i, j)
			if currentV < minV:
				minV = currentV
				minC = j
			board[i][j] = 0
		board[i][currentPos] = 1
		return minC

	#Solve this n-queens problem using a backtracking search strategy. We will start with row 0, then row 1, etc
	def BacktrackingSolver(self):
		self.board = np.zeros((self.board_size, self.board_size)) #Start with empty board
		vals = np.arange(self.board_size)
		domains = np.meshgrid(vals, vals)[0] #List of lists, domains[i] contains column locations for queen i not yet ruled out.

	def BacktrackingHelper(self, currentRow):
		for i in range(currentRow, self.board_size):
			for j in range(self.board_size):
				self.board[i][j] = 1
				if self.violated_constraints(i, j) == 0:
					continue
				self.board[i][j] = 0


testBoard = Board(4)
print(testBoard.board)
testBoard.solve()
print(testBoard.board)
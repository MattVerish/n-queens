import tkinter as tk
from tkinter import *
import random
import sklearn

class App(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.canvas = tk.Canvas(self, width=500, height=500, borderwidth=0, highlightthickness=0)
        self.canvas.pack(side="top", fill="both", expand="true")
        self.rows = 8
        self.columns = 8
        self.cellwidth = 50
        self.cellheight = 50

        img = PhotoImage(file="Queen.gif")
        self.rect = {}
        self.oval = {}
        for column in range(20):
            for row in range(20):
                x1 = column*self.cellwidth
                y1 = row * self.cellheight
                x2 = x1 + self.cellwidth
                y2 = y1 + self.cellheight
                if (column + row) % 2 == 0:
                    self.rect[row,column] = self.canvas.create_rectangle(x1,y1,x2,y2, fill="white", tags="rect")
                else:
                    self.rect[row,column] = self.canvas.create_rectangle(x1,y1,x2,y2, fill="gray", tags="rect")

                #self.oval[row,column] = self.canvas.create_oval(x1+2,y1+2,x2-2,y2-2, fill="green", tags="oval")
        self.canvas.pack()
        self.canvas.create_image(50, 50, image=img)

    def redraw(self, delay):
        self.canvas.itemconfig("rect", fill="black")
        self.canvas.itemconfig("oval", fill="blue")
        for i in range(10):
            row = random.randint(0,19)
            col = random.randint(0,19)
            item_id = self.oval[row,col]
            self.canvas.itemconfig(item_id, fill="green")
        self.after(delay, lambda: self.redraw(delay))

if __name__ == "__main__":
    app = App()
    app.mainloop()